import React from "react";

import "./styles.css";

const Spinner = () => (
  <div className="wrap-loader">
    <div className="loader">Loading...</div>
  </div>
);

export default Spinner;
