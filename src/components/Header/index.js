import React from "react";
import PropTypes from "prop-types";

import withSearch from "../../HOC/withSearch";

import Input from "../Input";

import S from "./styles";

const SearchInput = withSearch(Input);

const Header = ({ onSearchAutoNumber }) => {
  return (
    <S.Header>
      <SearchInput onSend={onSearchAutoNumber} />
    </S.Header>
  );
};

Header.propTypes = {
  onSearchAutoNumber: PropTypes.func
};

export default Header;
