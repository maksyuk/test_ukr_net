import styled from "styled-components";

export default {
  Header: styled.div`
    background: #3f51b5;
    height: 20vh;
    display: flex;
    justify-content: center;
    align-items: center;
  `
};
