import React from "react";
import PropTypes from 'prop-types';

import InfoTable from "../InfoTable";
import Spinner from "../Spinner";

import S from "./styles";

const MainInfoArea = ({
  errorMessage,
  children,
  infoByAutoNumber,
  isLoading
}) => {
  if (isLoading) {
    return (
      <S.Wrap>
        <Spinner />
      </S.Wrap>
    );
  }

  return (
    <S.Wrap>
      {infoByAutoNumber ? (
        <InfoTable infoByAutoNumber={infoByAutoNumber} />
      ) : errorMessage ? (
        <S.Error>{errorMessage}</S.Error>
      ) : (
        <S.Hint>Введите номер автомобиля в поле ввода выше</S.Hint>
      )}
    </S.Wrap>
  );
};

MainInfoArea.propTypes = {
    errorMessage: PropTypes.string,
    children: PropTypes.node,
    infoByAutoNumber: PropTypes.object,
    isLoading: PropTypes.bool,
};

export default MainInfoArea;
