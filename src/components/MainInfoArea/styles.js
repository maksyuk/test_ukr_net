import styled from "styled-components";

export default {
  Hint: styled.div`
    font-size: 30px;
    color: blue;
  `,
  Error: styled.div`
    font-size: 30px;
    color: red;
  `,
  Wrap: styled.div`
    height: 78vh;
    display: flex;
    justify-content: center;
    align-items: center;
  `
};
