import styled from "styled-components";

export default {
  Input: styled.input`
    width: 20%;
    height: 50px;
    font-size: 30px;
    width: 100%;
    letter-spacing: 4px;
    text-transform: uppercase;
  `,
  Wrap: styled.div`
    position: relative;
  `,
  Error: styled.div`
    position: absolute;
    color: #ef5350;
    font-weight: bold;
  `
};
