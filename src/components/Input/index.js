import React from "react";
import PropTypes from "prop-types";

import S from "./styles";

const Input = React.forwardRef(({ errorMessage, value, onChange }, ref) => (
  <S.Wrap>
    <S.Input ref={ref} type="text" value={value} onChange={onChange} />
    {errorMessage && <S.Error>{errorMessage}</S.Error>}
  </S.Wrap>
));

Input.propTypes = {
  errorMessage: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};


Input.defaultProps = {
  errorMessage: "",
  value: "",
  onChange: () => {}
};

export default Input;
