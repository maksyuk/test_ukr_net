import React from "react";
import PropTypes from 'prop-types';

import S from "./styles";

const InfoTable = ({ infoByAutoNumber }) => {
  const headers = Object.keys(infoByAutoNumber);
  const content = Object.values(infoByAutoNumber);
  return (
    <S.Table>
      <S.Thead>
        {headers.map(item => (
          <S.Td>{item}</S.Td>
        ))}
      </S.Thead>
      <S.Tbody>
        {content.map(item => (
          <S.Td>{item}</S.Td>
        ))}
      </S.Tbody>
    </S.Table>
  );
};

InfoTable.propTypes = {
    infoByAutoNumber: PropTypes.shape({
        crashesCount: PropTypes.number,
        owner: PropTypes.string,
        ownersCount: PropTypes.number,
        year: PropTypes.number
    })
}


export default InfoTable;
