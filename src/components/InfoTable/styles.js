import styled from "styled-components";

export default {
  Wrap: styled.div``,
  Table: styled.table`
    border: 1px solid black;
    font-size: 24px;
    border-collapse: collapse;
  `,
  Tr: styled.tr`
    border: 1px solid black;
  `,
  Td: styled.td`
    border: 1px solid black;
    text-align: center;
  `,
  Thead: styled.thead`
    & > td {
      padding: 20px;
      font-weight: bold;
    }

    & > td::first-letter {
      text-transform: uppercase;
    }
  `,
  Tbody: styled.tbody``
};
