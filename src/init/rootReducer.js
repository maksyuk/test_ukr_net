import { combineReducers } from "redux";
import { infoReducer as info } from "../bus/info/reducer";

export default combineReducers({
  info
});
