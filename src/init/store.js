import { createStore, applyMiddleware, compose } from 'redux';

import { middlewares, sagaMiddleware } from './middleware';

import rootReducer from './rootReducer';
import { rootSaga } from './rootSaga';

const store = createStore(rootReducer, compose(applyMiddleware(...middlewares)));

export { store };

sagaMiddleware.run(rootSaga);
