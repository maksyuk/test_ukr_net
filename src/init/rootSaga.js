import { all } from "redux-saga/effects";

import { infoWorkers } from "../bus/info/saga";

export function* rootSaga() {
  yield all([infoWorkers.watchFetchAutoNumberInfo()]);
}
