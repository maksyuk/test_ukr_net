const API_HOST = '//localhost:8080';
const API_VERSION = 'v1';

export const API = `${API_HOST}/api/${API_VERSION}`;

export const ERROR_AUTO_NUMBER = 'Введен неправильный номер';

export const ENTER_KEY_CODE = 13;
