import React from "react";
import throttle from "lodash.throttle";

import { ERROR_AUTO_NUMBER, ENTER_KEY_CODE } from "../helpers/constants";

export default Component =>
  class WithSearch extends React.Component {
    state = {
      error: "",
      value: ""
    };

    componentRef = React.createRef();

    validator = value => {
      const carNumberRegExp = /^[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}$/;
      return carNumberRegExp.test(value.toUpperCase());
    };

    onError = error => this.setState({ error });

    onChange = e => {
      const { value } = e.target;
      if (value.length > 8) return false;
      this.setState({ value, error: "" }, () => {
        if (!this.validator(value) && value.length === 8) {
          this.onError(ERROR_AUTO_NUMBER);
        }
      });
    };

    onEnter = throttle(e => {
      const { value, error } = this.state;
      const { onSend } = this.props;
      if (e.keyCode === ENTER_KEY_CODE && value.length === 8 && !error) {
        onSend && onSend(value.toUpperCase());
      }
    }, 500);

    componentDidMount() {
      this.componentRef.current.addEventListener("keypress", this.onEnter);
    }

    componentWillUnmount() {
      this.componentRef.current.removeEventListener("keypress", this.onEnter);
    }

    render() {
      const { value, error } = this.state;
      return (
        <Component
          {...this.props}
          value={value}
          onChange={this.onChange}
          errorMessage={error}
          ref={this.componentRef}
        />
      );
    }
  };
