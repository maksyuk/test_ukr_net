import React from 'react';
import { Provider } from 'react-redux';
import { store } from './init/store';

import AutoNumberInfoContainer from './containers/AutoNumberInfoContainer'

function App() {
  return (
    <Provider store={store}>
        <AutoNumberInfoContainer />
    </Provider>
  );
}

export default App;
