import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createStructuredSelector } from "reselect";
import PropTypes from "prop-types";

import Header from "../components/Header";
import MainInfoArea from "../components/MainInfoArea";

import {
  selectAutoNumberInfo,
  selectIsLoadingInfo,
  selectError
} from "../bus/info/selectors";
import { infoActions } from "../bus/info/actions";

const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoadingInfo(),
  infoByAutoNumber: selectAutoNumberInfo(),
  errorMessage: selectError()
});

const mapDispatchToProps = dispach => ({
  actions: bindActionCreators(
    {
      ...infoActions
    },
    dispach
  )
});

class AutoNumberInfoContainer extends React.Component {
  render() {
    const {
      actions: { fetchAutoNumberInfo },
      infoByAutoNumber,
      errorMessage,
      isLoading
    } = this.props;
    return (
      <React.Fragment>
        <Header onSearchAutoNumber={fetchAutoNumberInfo} />
        <MainInfoArea
          errorMessage={errorMessage}
          infoByAutoNumber={infoByAutoNumber}
          isLoading={isLoading}
        />
      </React.Fragment>
    );
  }

  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func),
    errorMessage: PropTypes.string,
    infoByAutoNumber: PropTypes.shape({
      crashesCount: PropTypes.number,
      owner: PropTypes.string,
      ownersCount: PropTypes.number,
      year: PropTypes.number
    })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AutoNumberInfoContainer);
