import types from "./types";

const initialState = {
  numberInfo: null,
  isLoad: false,
  errorMessage: ""
};

export const infoReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_AUTO_NUMBER_INFO_SUCCESS:
      return {
        ...state,
        numberInfo: payload,
        errorMessage: ""
      };
    case types.FETCH_AUTO_NUMBER_INFO_ERROR:
      return {
        ...state,
        errorMessage: "Что-то пошло не так, попробуйте еще раз"
      };
    case types.START_FETCH_AUTO_NUMBER_INFO:
      return {
        ...state,
        isLoad: true,
        numberInfo: null
      };
    case types.END_FETCH_AUTO_NUMBER_INFO:
      return {
        ...state,
        isLoad: false
      };
    default: {
      return state;
    }
  }
};
