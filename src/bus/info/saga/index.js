import { takeEvery } from "redux-saga/effects";
import { callFetchAutoNumberInfo } from "./workers";
import types from "../types";

export const infoWorkers = Object.freeze({
  *watchFetchAutoNumberInfo() {
    yield takeEvery(types.FETCH_AUTO_NUMBER_INFO, callFetchAutoNumberInfo);
  }
});
