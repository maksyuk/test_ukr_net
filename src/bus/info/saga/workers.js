import { call, put } from 'redux-saga/effects';

import { infoActions } from "../actions";

import { API } from "../../../helpers/constants";

export function* callFetchAutoNumberInfo({ payload: { autoNumber } }) {
    try {
        yield put(infoActions.startFetchAutoNumberInfo())
        const response = yield call(fetch, `${API}/car-info/${autoNumber}`, {
            method:  "GET",
        });

        const data = yield call([response, response.json]);
        // yield delay(2000); //for test spinner

        if(data.hasOwnProperty('error') || !data.hasOwnProperty('result')) {
            throw new Error();
        }

        yield put(infoActions.fetchAutoNumberInfoSuccess(data.result))
    }
    catch (e) {
        yield put(infoActions.fetchAutoNumberInfoError())
    }
    finally {
        yield put(infoActions.endFetchAutoNumberInfo())
    }
}
