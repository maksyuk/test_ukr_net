import types from "./types";
export const infoActions = Object.freeze({
  fetchAutoNumberInfo: autoNumber => ({
    type: types.FETCH_AUTO_NUMBER_INFO,
    payload: { autoNumber }
  }),
  fetchAutoNumberInfoSuccess: info => ({
    type: types.FETCH_AUTO_NUMBER_INFO_SUCCESS,
    payload: info
  }),
  fetchAutoNumberInfoError: () => ({
    type: types.FETCH_AUTO_NUMBER_INFO_ERROR
  }),
  startFetchAutoNumberInfo: () => ({
    type: types.START_FETCH_AUTO_NUMBER_INFO
  }),
  endFetchAutoNumberInfo: () => ({ type: types.END_FETCH_AUTO_NUMBER_INFO })
});
