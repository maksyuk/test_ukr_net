import { createSelector } from 'reselect';

export const mainSelector = ({ info }) => info;

export const selectAutoNumberInfo = () => createSelector(mainSelector, ({ numberInfo }) => numberInfo);
export const selectIsLoadingInfo = () => createSelector(mainSelector, ({ isLoad }) => isLoad);
export const selectError = () => createSelector(mainSelector, ({ errorMessage }) => errorMessage);

